package controlador;


import java.util.List;

import modelo.data.Carro;
import modelo.servicio.IServicio;
import modelo.servicio.Servicio;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.*;
import org.zkoss.zul.*;

public class ControladorBusquedaCarros extends SelectorComposer<Component> {

	private static final long serialVersionUID = 1L;
	
	@Wire
	private Textbox txtPalabraClave;
	@Wire
	private Listbox lstListaCarros;
	@Wire
	private Label lblModelo;
	@Wire
	private Label lblMarca;
	@Wire
	private Label lblPrecio;
	@Wire
	private Label lblDescripcion;
	@Wire
	private Image imgImagenPrevia;
	
	
	private IServicio servicioCarro = new Servicio();
	
	@Listen("onClick = #btnBuscar")
	public void search(){
		String keyword = txtPalabraClave.getValue();
		List<Carro> result = servicioCarro.search(keyword);
		lstListaCarros.setModel(new ListModelList<Carro>(result));
	}
	
	@Listen("onSelect = #lstListaCarros")
	public void showDetail(){
		Carro selected = lstListaCarros.getSelectedItem().getValue();
		imgImagenPrevia.setSrc(selected.getPreview());
		lblModelo.setValue(selected.getModelo());
		lblMarca.setValue(selected.getMarca());
		lblPrecio.setValue(selected.getPrecio().toString());
		lblDescripcion.setValue(selected.getDescripcion());
	}
	
	public String gitPrueba(){
		
		return "Soy un mensaje de GIT";
	}
}
