package modelo.servicio;


import java.util.List;

import modelo.data.Carro;

public interface IServicio {

	/**
	 * Retrieve all cars in the catalog.
	 * @return all cars
	 */
	public List<Carro> findAll();
	
	/**
	 * search cars according to keyword in model and make.
	 * @param keyword for search
	 * @return list of car that match the keyword
	 */
	public List<Carro> search(String keyword);
}
