package modelo.servicio;


import java.util.LinkedList;
import java.util.List;

import modelo.data.Carro;

public class Servicio implements IServicio{

	//data model
	private List<Carro> listaCarro= new LinkedList<Carro>();
	private static int id = 1;
	//initialize book data
	public Servicio() {
		listaCarro.add(
				new Carro(id++, 
						"Primera",
						"Nissan",
						"El Nissan Primera se produjo entre 2002 y 2008. Estaba disponible como un sedán de 4 puertas o hatchback de 5 puertas."+
						" La gasolina de 1.6 litros de nivel de entrada se siente el poder suficiente para un carro grande. La gasolina de 1.8 litros tiene mucho interés, la unidad de 2.0 litros refinado es el producto estrella. Un turbodiesel de 2.2 litros mejorado funciona bien, pero es sólo relativamente económico, ya que esta clase de características similares ofrecen aún más bajo consumo de combustible.",
						"img/car1.png",
						23320));
		listaCarro.add(
				new Carro(id++, 
						"Cefiro",
						"Nissan",
						"El Nissan Cefiro es una gama de automóviles de tamaño intermedio se vende en Japón y otros países. Sólo estaba disponible como un sedán de 4 puertas. Una gran parte fueron equipados con transmisiones automáticas. El modelo superior utiliza el mismo motor que se encuentra en el R32 Nissan Skyline, a 2 litros turbo cargado motor de 6 cilindros capaz de algo más de 200 caballos de fuerza (150 kW).",
						"img/car2.png",
						38165));
		listaCarro.add(
				new Carro(id++, 
						"Camry",
						"Toyota",
						"El Toyota Camry es un auto mediano fabricado por Toyota en Georgetown, Kentucky, EE.UU., así como Australia y Japón. Desde el año 2001 ha sido el coche más vendido en EE.UU. Desde el año 2000 Daihatsu ha vendido un Camry gemelo nombrado el Daihatsu Altis",
						"img/car3.png",
						24170));
		listaCarro.add(
				new Carro(id++, 
						"Century",
						"Toyota",
						"El Century Toyota es una gran limusina de cuatro puertas, producido por Toyota principalmente para el mercado japonés. La producción del siglo comenzó en 1967 y el modelo recibió sólo cambios menores hasta rediseño en 1997. Este Century de segunda generación todavía se vende en Japón.",
						"img/car4.png",
						28730));
		listaCarro.add(
				new Carro(id++, 
						"Sigma",
						"Mitsubishi",
						"La tercera generación de la automotriz japonesa Mitsubishi Galant, que data de 1976, se dividió en dos modelos: el Galant Sigma (para el sedán y wagon) y el Galant Lambda (el coupé). El primero fue vendido en muchos mercados como el Mitsubishi Galant (sin la palabra 'Sigma') y en Australia como el Chrysler Sigma (hasta el año 1980, después de lo cual se convirtió en el Mitsubishi Sigma).",
						"img/car5.png",
						54120));
		listaCarro.add(
				new Carro(id++, 
						"Challenger",
						"Mitsubishi", 
						"El Mitsubishi Challenger, llamado Mitsubishi Pajero Sport en la mayoría de los mercados de exportación, Mitsubishi Montero Sport en los países de habla española (incluyendo América del Norte), Mitsubishi Shogun Sport en el Reino Unido y Mitsubishi Nativa de América Central y del Sur (el nombre Challenger también se utiliza en Australia ), es un SUV de tamaño medio construido por Mitsubishi Motors Corporation. Fue lanzado en 1997, aunque ya no está disponible en su país natal, Japón desde el final de 2003.",
						"img/car6.png",
						58750));
		listaCarro.add(
				new Carro(id++, 
						"Civic",
						"Honda",
						"La octava generación de Honda Civic se produce desde el año 2006. Está disponible como un coupé, hatchback y sedán. Modelos producidos para el mercado de América del Norte tienen un estilo diferente. En este momento hay cuatro motores de gasolina y uno diésel desarrollado para este coche. La gasolina de 1.4 litros es más adecuado para la conducción.",
						"img/car1.png",
						17479));
		listaCarro.add(
				new Carro(id++, 
						"Golf V",
						"Volkswagen",
						"El Volkswagen Golf V se produce desde el año 2003. Hay una amplia gama de motores finos y confiable. La mejor opción sería la de 1.6 litros de gasolina de inyección directa de gasolina FSI con 115 CV o 2.0 litros turbodiesel con 150 caballos de fuerza. El último mencionado cuenta con una economía de combustible excepcional por su capacidad y velocidad de aceleración, aunque es un poco ruidoso. El mejor desempeño es un GTI 2.0 litros, entregando 200 caballos de fuerza, que sigue las tradiciones de escotilla caliente del golf.",
						"img/car3.png",
						78200));
		listaCarro.add(
				new Carro(id++, 
						"Neon",
						"Chrysler",
						"Este coche se vende como el Dodge Neon en los Estados Unidos y como Chrysler Neon sólo para exportación. Se trata de una segunda generación de neón producido desde el año 2000."+
						" Hay una opción de tres motores de gasolina. La gasolina de 1.6 litros básica es la misma unidad que se encuentra en el MINI. La parte superior de la gama es un motor de 2.0 litros, que proporciona 133 caballos de fuerza, además de la aceleración es un punto débil de todos los neones.",
						"img/car4.png",
						85400));
	}
	
	
	public List<Carro> findAll(){
		return listaCarro;
	}
	
	public List<Carro> search(String palabraClave){
		List<Carro> result = new LinkedList<Carro>();
		if (palabraClave==null || "".equals(palabraClave)){
			result = listaCarro;
		}else{
			for (Carro c: listaCarro){
				if (c.getModelo().toLowerCase().contains(palabraClave.toLowerCase())
					||c.getMarca().toLowerCase().contains(palabraClave.toLowerCase())){
					result.add(c);
				}
			}
		}
		return result;
	}
}
