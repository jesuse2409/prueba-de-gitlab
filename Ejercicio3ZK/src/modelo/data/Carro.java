package modelo.data;


public class Carro {

	private Integer id;
	private String modelo;
	private String marca;
	private String preview;
	private String descripcion;
	private Integer precio;
	private String var;
	
	public Carro(){
	}
	
	public Carro(Integer id, String modelo, String marca, String descripcion,  String preview, Integer precio){
		this.id = id;
		this.modelo = modelo;
		this.marca = marca;
		this.preview = preview;
		this.descripcion = descripcion;
		this.precio = precio;
	}
	
	public void setVar(String a) {
		var= a;
		
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getPreview() {
		return preview;
	}
	public void setPreview(String preview) {
		this.preview = preview;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

}
